**Repository for data analysis of "Network-level modeling of stimulation-induced functional connectivity change: An optogenetic study in non-human primate cortex"**

---

## Repository Format

The data analysis is completed in dedicated jupyter notebooks and R code. Data is included in the figshare link in the manuscript.

---